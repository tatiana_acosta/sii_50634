// MundoCliente.cpp:
// Modificado por-> Tatiana Acosta Lazaro .Num.Matricula->50634
/////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
/*
	//Cierre del fd fifo
	datoescrito[1]=3;
	write(fdfifo,&datoescrito,sizeof(int[2]));
	error_cierrefdfifo=close(fdfifo);
	if (error_cierrefdfifo==-1)
		{
			perror("Erro al hacer el close del fd del fifo");
			exit(1);
		}
	 close(fdfifo);
*/
	//Cierre de la proyección de memoria
	
	error_cierreproyeccion=munmap(proyeccion,sizeof(Memoria));
	if (error_cierreproyeccion==-1)
		{
			perror("Erro al hacer la desproyeccion del fichero compartido");
			exit(1);
		}
//tuberia coordendas
	close(fdfifoCoor);
	unlink("/tmp/fifocoordenadas");
//tuberia teclas	
	close(fd_fifoKey);
	unlink("/tmp/fifoKey");
}



void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
/*
//Impresion para comprobacion in situ	
	sprintf(cad,"Accion del primer jugador: %d",pMemoria->accion);
	print(cad,300,0,1,1,1);	
*/
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	//Rebotes de paredes y esfera o jugadores	
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	if(jugador1.Rebota(esfera))
		{
		numero_rebotes++;
		esfera.velocidad.y=esfera.velocidad.y+10;
		esfera.velocidad.x=esfera.velocidad.x+2;
		}
	//Rebote de jugador 2 y esfera
	if(jugador2.Rebota(esfera))
		{
		numero_rebotes++;
		esfera.velocidad.y=esfera.velocidad.y+2;
		esfera.velocidad.x=esfera.velocidad.x+15;
		}

	//Implementacion variacion del radio de esfera en funcion de num de rebotes
	if(numero_rebotes>0)
		{
		if(numero_rebotes>0&&jugador2.Rebota(esfera)&&esfera.radio>0.02f)
			esfera.radio=esfera.radio-0.09;
		if(numero_rebotes>0&&jugador1.Rebota(esfera)&&esfera.radio>0.02f)
			esfera.radio=esfera.radio-0.02;
			
		}
	//Implementacion sumar puntos jugador2 al golpear pared izq
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		numero_rebotes=0;
/*	
		//Escritura por el fifo
		datoescrito[0]=2;
		datoescrito[1]=puntos2;
		nescritosfifo=write(fdfifo,&datoescrito,sizeof(int[2]));
		if (nescritosfifo==-1)
		{
			perror("Erro al escribir en el fifo");
			close(fdfifo);
			exit(1);
		}
		if (nescritosfifo!=sizeof(int[2]))
		{
			perror("Erro: numero escrito en el fifo es distinto de int[2]");
			close(fdfifo);
			exit(1);
		}
*/


	}
	//Implementacion sumar puntos jugador1 al golpear pared dch
	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		numero_rebotes=0;
/*
		//Escritura por el fifo
		datoescrito[0]=1;
		datoescrito[1]=puntos1;
		nescritosfifo=write(fdfifo,&datoescrito,sizeof(int[2]));
		if (nescritosfifo==-1)
		{
			perror("Erro al escribir en el fifo");
			close(fdfifo);
			exit(1);
		}
		if (nescritosfifo!=sizeof(int[2]))
		{
			perror("Erro: numero escrito en el fifo es distinto de int[2]");
			close(fdfifo);
			exit(1);
		}
*/
	}
///////////////////////////
//Lectura del fifo coordenadas		
		nleidosfifoCoor=read(fdfifoCoor,&coordenadas,sizeof(coordenadas));
		sscanf(coordenadas,"%f %f %f %f %f %f %f %f %f %f %d %d",&esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1 , &puntos2);
		if (nleidosfifoCoor==-1)
			{
				perror("Erro al leer el fifo de coordenadas en muncoClente.cpp");
				close(fdfifoCoor);
				exit(1);
			}
		
		if (nleidosfifoCoor!=sizeof(coordenadas)&&nleidosfifoCoor!=-1)
			{
				perror("Erro: numero leido del fifo de coordenadas en muncoClente.cpp es distinto de coordenadas[200]");
				close(fdfifoCoor);
				exit(1);
			}
//////////////////////////////

	if(jugador1.Rebota(esfera)){

		jugador1.y1-=0.2;
		jugador2.y1+=0.2;

	}

	if(jugador2.Rebota(esfera)){

		jugador1.y1+=0.2;
		jugador2.y1-=0.2;

	}
	//Proyectamos los datos reales del bot a Mundo
	pMemoria->esfera=esfera;
	pMemoria->raqueta1=jugador1;

        //Implementacion de las acciones definidas en el bot con el OnkeyboardDown
	switch(pMemoria->accion)

	{

		case 1: OnKeyboardDown('w',0,0); break;
		case 0: jugador1.velocidad.y=0;break;
		case -1: OnKeyboardDown('s',0,0); break;

	}
	
	//Finalizacion del juego porque puntos==3

	if(puntos1==3 | puntos2==3)
		{	
		//usleep(2500);
		pMemoria->accion=3;
		//usleep(2500);	
		exit(0);
		}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':
		if(puntos1>4|puntos2>4)
			{jugador1.velocidad.y=-8;break;}
		if(puntos1<4|puntos2<4)
			{jugador1.velocidad.y=-4;
				break;}
		
	case 'w':
		if(puntos1>4|puntos2>4)
			{jugador1.velocidad.y=8;break;}
		if(puntos1<4|puntos2<4)
		{jugador1.velocidad.y=4;
				break;}
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
//char tecla[5];
	sprintf(tecla,"%c",key);
	write(fd_fifoKey,tecla,sizeof(tecla));
}

void CMundo::Init()
{
	numero_rebotes=0;
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
/*
	//Informacion Sobre el Logger
	fdfifo=open("/tmp/fifo",O_WRONLY);
	if (fdfifo==-1)
	{
		perror("Erro al abrir el fifo en mundo.cpp");
		close(fdfifo);
		exit(1);
	}
*/

//Fichero proyectado en memoria

	//Creacion del fichero
	fd_mcompartida=open("/tmp/datoscompartidos1.txt",O_RDWR|O_CREAT|O_TRUNC, 0777);
	if (fd_mcompartida==-1)
		{
			perror("Erro al abrir memoria compartida desde mundoCliente");
			exit(1);
		}

	//Escribimos en el fichero
	write(fd_mcompartida,&Memoria,sizeof(Memoria));

	//Proyectamos el fichero compartido
	//char* proyeccion;
	proyeccion=(char*)mmap(NULL,sizeof(Memoria),PROT_WRITE|PROT_READ,MAP_SHARED,fd_mcompartida,0);
	if (proyeccion==(void *)MAP_FAILED)
		{
			perror("Erro al proyectar memoria compartida desde mundoCliente");
			close(fd_mcompartida);
			exit(1);
		}
	//Cerramos el descriptor de fichero de memoria compartida

	error_cierrefdmem=close(fd_mcompartida);
	if (error_cierrefdmem==-1)
		{
			perror("Erro al cerrar el fd de memoria compartida desde mundoCliente");
			exit(1);
		}	


	//Asignamos el valor del puntero de proyeccion al puntero de Datos

	pMemoria=(DatosMemCompartida*)proyeccion;
	pMemoria->accion=0;

//Tuberia coordenadas MundoServidor
	/*Creacion del fifo*/
		error_mkfifoCoor=mkfifo("/tmp/fifocoordenadas",0777);
	/*Comprobacion del error de creacion*/
	
		if (error_mkfifoCoor==-1)
			{
				perror("Erro al crear el fifo de coordenadas en MundoCliente.cpp");
				unlink("/tmp/fifocoordenadas");
				exit(1);
			}
	/*Apertura del fifo*/
		fdfifoCoor=open("/tmp/fifocoordenadas", O_RDONLY);
		if (fdfifoCoor==-1)
		{
			perror("Erro al abrir el fifo de coordenadas en MundoCliente.cpp");
			close(fdfifoCoor);
			exit(1);
		}
//fifo teclas
	/*Creacion del fifo*/
		error_mkfifoKey=mkfifo("/tmp/fifoKey",0777);
	/*Comprobacion del error de creacion*/
	
		if (error_mkfifoKey==-1)
			{
				perror("Erro al crear el fifo KEy en MundoCliente.cpp");
				unlink("/tmp/fifoKey");
				exit(1);
			}
	/*Apertura del fifo*/
		fd_fifoKey=open("/tmp/fifoKey", O_WRONLY);
		if (fd_fifoKey==-1)
		{
			perror("Erro al abrir el fifo Key en muncoCliente.cpp");
			close(fd_fifoKey);
			exit(1);
		}
}
