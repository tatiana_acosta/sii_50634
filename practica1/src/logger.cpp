//////////////////// logger.cpp                                               /////////
//////////////////// Hecho por -> Tatiana Acosta Lazaro, num. matricula ->50634////////
///////////////////////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
using namespace std;

#pragma one

int main (int argc, char* argv[])
{
	//variables para el fifo del logger con mundoServidor.cpp
	int fd_fifo;
	int error_mkfifo;
	int nleidosfifo;
	int datoleido[2];
	int error_cierrefdfifo;
	int error_cierrefifo;
	
	//fifo
	/*Creacion del fifo*/
		error_mkfifo=mkfifo("/tmp/fifologger",0777);
	/*Comprobacion del error de creacion*/
	
		if (error_mkfifo==-1)
			{
				perror("Erro al crear el fifo de logger en logger.cpp");
				unlink("/tmp/fifologger");
				exit(1);
			}
	/*Apertura del fifo*/
		fd_fifo=open("/tmp/fifologger", O_RDONLY);
		if (fd_fifo==-1)
		{
			perror("Erro al abrir el fifo en logger.cpp");
			close(fd_fifo);
			exit(1);
		}
	/*Lectura del fifo*/
		while(1)
		{
			nleidosfifo=read(fd_fifo,&datoleido,sizeof(int[2]));
			
			if (nleidosfifo==-1)
				{
					perror("Erro al leer el fifo de logger en logger.cpp");
					close(fd_fifo);
					exit(1);
				}
			
			if (nleidosfifo!=sizeof(int[2]))
				{
					perror("Erro: numero leido del fifo de logger en logger.cpp es distinto de int[2]");
					close(fd_fifo);
					exit(1);
				}
			
			if(datoleido[0]==1)
				{
				cout<< "Jugador 1 marca 1 punto, lleva un total de "<< datoleido[1]<<endl;
				if(datoleido[1]==3) // Para cerrar el descriptor y la tuberia y salir
				{	
					cout<<"---Cerrando fifo del logger en logger.cpp porque el jugador1 lleva 3 puntos y gana la partida ---"<<endl;
					error_cierrefdfifo=close(fd_fifo);//cierre fd fifo
					if (error_cierrefdfifo==-1)
						{
							perror("Erro al hacer el close del fd del fifo del logger en logger.cpp");
							exit(1);
						}
					error_cierrefifo=unlink("/tmp/fifo");//cierre bufer fifo
					if (error_cierrefifo==-1)
						{
							perror("Erro al hacer el unlink del fifo del logeer en logger.cpp");
							exit(1);
						}
					exit(1);				
				}
				}
			if(datoleido[0]==2)
				{
					cout<< "Jugador 2 marca 1 punto, lleva un total de "<< datoleido[1]<<endl;
				if(datoleido[1]==3) // Para cerrar el descriptor y la tuberia y salir
				{	
					cout<<"---Cerrando fifo logger porque el jugador2 lleva 3 puntos y gana la partida ---"<<endl;
					error_cierrefdfifo=close(fd_fifo);//cierre fd fifo
					if (error_cierrefdfifo==-1)
						{
							perror("Erro al hacer el close del fd del fifo del logger en logger.cpp");
							exit(1);
						}
					error_cierrefifo=unlink("/tmp/fifo");//cierre bufer fifo
					if (error_cierrefifo==-1)
						{
							perror("Erro al hacer el unlink del fifo del logeer en logger.cpp");
							exit(1);
						}
					exit(1);				
				}
				}
			sleep(2);
			
		
}
	/*Cierre del fd de fifo*/
	close(fd_fifo);
	if (error_cierrefdfifo==-1)
		{
			perror("Erro al hacer el close del fd del fifo");
			exit(1);
		}
	/*Deslink del fifo*/
	error_cierrefifo=unlink("/tmp/fifo");//cierre bufer fifo
		if (error_cierrefifo==-1)
			{
				perror("Erro al hacer el unlink del fifo");
				exit(1);
			}
	return(0);

}
