//////////////////// bot.cpp                                               /////////
//////////////////// Hecho por -> Tatiana Acosta Lazaro, num. matricula ->50634////////
///////////////////////////////////////////////////////////////////////////////////////



#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>

#include <unistd.h>
#include "DatosMemCompartida.h"
#include "Esfera.h"
using namespace std;

int main()

{
	DatosMemCompartida* datocompartido;
	int fd_mcompartida;
	int error_cierrefdmem;
	int error_cierreproyeccion;
	int centrojugador1;
	char* proyeccion;
	int continua=1;
	

	//Abrimos el fichero
	fd_mcompartida=open("/tmp/datoscompartidos1.txt", O_RDWR);
	if (fd_mcompartida==-1)
		{
			perror("Erro al abrir memoria compartida desde bot");
			exit(1);
		}

	//Proyectamos el fichero
	proyeccion=(char*)mmap(NULL, sizeof(*(datocompartido)),PROT_READ|PROT_WRITE, MAP_SHARED,fd_mcompartida,0);
	if (datocompartido==(void *)MAP_FAILED)
		{
			perror("Erro al proyectar memoria compartida desde bot");
			close(fd_mcompartida);
			exit(1);
		}

	/*Cierre de fd del fichero compartido*/
	error_cierrefdmem=close(fd_mcompartida);
	if (error_cierrefdmem==-1)
		{
			perror("Erro al cerrar el fd de memoria compartida desde bot");
			exit(1);
		}	

	//Apuntamos nuestro puntero de Datos a la proyeccion del fichero en memoria

	datocompartido=(DatosMemCompartida*)proyeccion;

	//Acciones de control de la raqueta

	while(continua==1)
	{

		float posJugador1;
		posJugador1=((datocompartido->raqueta1.y2+datocompartido->raqueta1.y1)/2);
		
		if(posJugador1<(datocompartido->esfera.centro.y))
			datocompartido->accion=1;



		if(posJugador1>(datocompartido->esfera.centro.y))
			datocompartido->accion=-1;
		else
			datocompartido->accion=0;
		usleep(2500);
		//Implementamos la finalizacion por maximos de puntos
		if(datocompartido->accion==3)
			continua==0;
		if (continua==0)
			{	
				munmap(proyeccion,sizeof(*(datocompartido)));
				cout<<"----Cerrando bot----"<<endl;
				exit(1);
			}		

	}

	//Desmontamos la proyeccion de memoria

	error_cierreproyeccion=munmap(proyeccion,sizeof(*(datocompartido)));
	if (error_cierreproyeccion==-1)
		{
			perror("Erro al hacer la desproyeccion del fichero compartido");
			exit(1);
		}
return (1);


}
