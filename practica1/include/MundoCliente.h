// MundoCliente.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"

//Includes tuberias

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
//Includes memoria compartida bot

#include "DatosMemCompartida.h"
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>


class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	int numero_rebotes;

//tuberia coordenadas

	int fdfifoCoor;
	int nleidosfifoCoor;
	char coordenadas[200];
	int error_cierrefdfifoCoor;
	int error_cierrefifoCoor;
	int error_mkfifoCoor;
//Tuberia teclas
	int fd_fifoKey;
	int error_mkfifoKey;
	int nleidosfifoKey;
	char tecla[5];
	int error_cierrefdfifoKey;
	int error_cierrefifoKey;

//Creacion del atributo de memoria 
	char* proyeccion;
	int fd_mcompartida;
	int error_cierrefdmem;
	int error_cierreproyeccion;
	DatosMemCompartida Memoria;
	DatosMemCompartida* pMemoria;

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
